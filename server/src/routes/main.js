import express from "express";

const router = express.Router();

router.get("/", (_req, res) => {
  res.send("Server is running! <3");
});

export default router;
