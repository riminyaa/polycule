import express from "express";
import dotenv from "dotenv";
import cors from "cors";

import indexRouter from "./routes/main.js";
//import createAccountTable from "./utils/createDB.js";

const app = express();

dotenv.config();
const port = process.env.APP_PORT || 8080;

app.use(
  express.urlencoded({
    extended: false,
  }),
);
app.use(express.json());
app.use(cors());

app.use("/", indexRouter);

//await createAccountTable();


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});
