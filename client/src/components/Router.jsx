import React from "react";
import { Routes, Route } from "react-router-dom";
import Connections from "./Connections/Connections";
import Profile from "./Profile/Profile";

const reactRouter = () => (
  <Routes>
    <Route path="/" element={<Connections />} />
    <Route path="/profile" element={<Profile />} />
    <Route path="*" element={<>Page not Found</>} />
  </Routes>
);

export default reactRouter;
