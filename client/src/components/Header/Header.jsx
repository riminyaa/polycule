import ResponsiveAppBar from "./ResponsiveAppBar";

const Header = () => <ResponsiveAppBar />;

export default Header;
