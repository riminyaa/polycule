import { Box, ListItem } from "@mui/material";
import ConnectiondCard from "./ConnectionCard";

const ConnectionsContainer = ({ connections }) => (
  <Box sx={{ display: "grid", gridTemplateRows: "repeat(3, 1fr)" }}>
    {connections.map((connection) => (
      <ListItem key={connection.id}>
        <ConnectiondCard key={connection.id} content={connection} />
      </ListItem>
    ))}
  </Box>
);

export default ConnectionsContainer;
