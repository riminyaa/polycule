import { useState } from "react";
import {
  Button,
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
} from "@mui/material";

export default function AddConnectionsForm({ addConnection }) {
  const [name, setName] = useState("");
  const [type, setType] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    addConnection({
      name,
      type,
      metas: [],
      id: Math.floor(Math.random() * Date.now()),
    });
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleTypeChange = (e) => {
    setType(e.target.value);
  };

  // TODO: Tähän haku, jotta voi valita tietokannassaolevista henkilöistä
  return (
    <form onSubmit={handleSubmit}>
      <FormHelperText id="connectiont">
        Here you can add your relationships.
      </FormHelperText>
      <FormControl>
        <div>
          <InputLabel htmlFor="name">Name</InputLabel>
          <Input
            id="name"
            aria-describedby="enter the name of the connection"
            value={name}
            onChange={handleNameChange}
          />
        </div>
      </FormControl>
      <FormControl>
        <div>
          <InputLabel htmlFor="type">Type</InputLabel>
          <Input
            id="type"
            aria-describedby="enter the type of the connection"
            value={type}
            onChange={handleTypeChange}
          />
        </div>
      </FormControl>
      <Button type="submit">Submit</Button>
    </form>
  );
}
