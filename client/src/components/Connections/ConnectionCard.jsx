import { useState } from "react";
import Box from "@mui/material/Box";
import ConnectionData from "./ConnectionData";
import AppCard from "../UI/AppCard";

export default function ConnectionCard({ content }) {
  const [isVissible, setVisibility] = useState(false);
  const handleClick = () => setVisibility(!isVissible);

  const data = ConnectionData(
    { name: content.name,
      type: content.type,
      metas: content.metas,
      isVissible,
      handleClick },
  );

  return (
    <Box sx={{ minWidth: 275, maxWidth: 500 }}>
      <AppCard data={data} />
    </Box>
  );
}
