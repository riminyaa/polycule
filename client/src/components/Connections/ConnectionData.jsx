import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const ConnectionData = ({ name, type, metas, isVissible, handleClick }) => (
  <>
    <CardContent>
      <Typography variant="h5" component="div">
        {name}
      </Typography>
      <Typography sx={{ mb: 1.5 }} color="text.secondary">
        {type}
      </Typography>
      {isVissible ? (
        <ul>
          {metas.map((meta) => (
            <li key={Date.now() * Math.random()}>{meta}</li>
          ))}
        </ul>
      ) : null}
    </CardContent>
    <CardActions>
      <Button size="small" onClick={handleClick}>
        {isVissible ? "Hide" : "More"}
      </Button>
    </CardActions>
  </>
);

export default ConnectionData;
