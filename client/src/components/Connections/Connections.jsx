import { useEffect, useState } from "react";
import ConnectionTabs from "./ConnectionTabs";

const ConnectionsContainer = () => {
  const [connections, setConnetions] = useState([]);

  useEffect(() => {
    const timer = setInterval(()=>console.log("TIMER!"), 1000);
    setConnetions([
      { id: 1, name: "Someone", type: "gf", metas: ["Someone"] },
      {
        id: 2,
        name: "Someone",
        type: "D",
        metas: ["Someone", "Someone", "Someone"],
      },
    ]);
    return () => clearInterval(timer);
  }, []);

  const addConnection = (connection) => {
    setConnetions((prev) => [...prev, connection]);
  };

  return <ConnectionTabs connections={connections} add={addConnection} />;
};

export default ConnectionsContainer;
