import * as React from "react";
import PropTypes from "prop-types";
import { Box, Tab, Tabs } from "@mui/material";
import ConnectionsContainer from "./ConnectionsContainer";
import AddConnectionsForm from "./AddConnectionsForm";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function ConnectionTabs({ connections, add }) {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const addNewConnection = (connection) => {
    add(connection);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="My Connections" {...a11yProps(0)} />
          <Tab label="Add Connection" {...a11yProps(1)} />
          <Tab label="Calendar" {...a11yProps(2)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <ConnectionsContainer connections={connections} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <AddConnectionsForm addConnection={addNewConnection} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        I do not really know if I ever use this.
      </TabPanel>
    </Box>
  );
}
