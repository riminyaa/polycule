import Card from "@mui/material/Card";
import { Paper } from "@mui/material";

const AppCard = ({ data }) => (
  <Paper elevation={2}>
    <Card variant="outlined">
      {data}
    </Card>
  </Paper>
);

export default AppCard;
