import { createContext, useEffect, useMemo, useState } from "react";

export const UserContext = createContext({
  loggedIn: false,
  user: "",
  onLogIn: () => {},
  onLogOut: () => {},
});

export const UserContextProvider = ({ children }) => {
  const [user, setUser] = useState("");
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    if (document.cookie.includes("app-token")) {
      setLoggedIn(true);
    }
  }, []);

  const onLogIn = (u) => {
    setLoggedIn(true);
    setUser(u);
  };

  const onLogOut = () => {
    setLoggedIn(false);
    setUser("");
    document.cookie = "app-token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  };

  const contextValues = useMemo(() => ({
    loggedIn,
    user,
    onLogIn,
    onLogOut,
  }), [user, loggedIn]);

  return (
    <UserContext.Provider
      value={contextValues}
    >
      {children}
    </UserContext.Provider>
  );
};
